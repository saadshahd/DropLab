/* global droplab */
droplab.plugin(function init(DropLab) {

  var keydown = function keydown(e) {
    var list = e.detail.hook.list;
    var data = list.data;
    var value = e.detail.hook.trigger.value.toLowerCase();
    var config;
    var matches = [];
    // will only work on dynamically set data
    if(!data){
      return;
    }
    config = droplab.config[e.detail.hook.id];
    matches = data.map(function(o){
      // cheap string search
      o.droplab_hidden = o[config.text].toLowerCase().indexOf(value) === -1;
      return o;
    });
    list.render(matches);
  }

  window.addEventListener('keyup.dl', keydown);
});